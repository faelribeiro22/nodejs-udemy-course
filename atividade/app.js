const http = require('http');
const fs = require('fs');

const users = [];

const server = http.createServer((req, res) => {
    const { url, method } = req;

    if(url === '/') {
        res.setHeader('Content-Type', 'text/html');
        res.write('<html>');
        res.write('<head><title>Create User</title></head>');
        res.write('<body><form action="/create-user" method="POST"><input type="text" name="username"/><button type="submit">Send!</button></form></body>');
        res.write('</html>');
        return res.end();
    }
    if (url === '/create-user' && method === 'POST') {
        const body = [];

        req.on('data', (chunk) => {
            body.push(chunk);
        });

        req.on('end', () => {
            const parsedBody = Buffer.concat(body).toString();
            const value = parsedBody.split('=')[1];
            users.push({username: value});
            fs.readFile('atividade/users.json', 'utf-8', (err, data) => {
                if (err && err.code && err.code === 'ENOENT' || data === '') {
                    const usersJSON = {
                        users: [
                            { username: value }
                        ]
                    };
                    fs.writeFileSync('users.json', JSON.stringify(usersJSON));
                } else {
                    fs.appendFileSync('users.json', {username: value});
                }
            });
        });
        res.statusCode = 302;
        res.setHeader('Location', '/');
        return res.end();
    }
    if (url === '/users') {
        res.setHeader('Content-Type', 'text/html');
        res.write('<html>');
        res.write('<head><title>List</title></head>');
        res.write('<body>');
        res.write('<ul>');
        users.forEach(element => {
            res.write(`<li>${element.username}</li>`)
        });
        res.write('</ul>');
        res.write('</body>');
        res.write('</html>');
        return res.end();
    }
    res.setHeader('Content-Type', 'text/html');
    res.write('<html>');
    res.write('<head><title>My First Page!</title></head>');
    res.write('<body>Hello from my node.js server!</body>');
    res.write('</html>');
    res.end();
});

server.listen(3000);
